from flask import Flask
from flask import render_template

app = Flask(__name__)

@app.route('/')
def index():
    return render_template("index.html")

@app.route('/home')
def index2():
    return "<H3> Hello From Home </H3>"


app.run()


